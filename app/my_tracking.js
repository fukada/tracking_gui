
var my_tracking = function (discan_id, hcan_id) {
    //画像の縦横幅
    var pic_height, pic_width, array_length;
    this.W = 0; this.H = 0;
    var constructor = function () {
        c = document.getElementById(discan_id);
        hc = document.getElementById(hcan_id);
        cc = c.getContext("2d");
        hcc = hc.getContext("2d");
    }

    this.read_movie = function (bmparr, num, W, H) {
        this.H = pic_height = H;
        this.W = pic_width = W;
        array_length = pic_width * pic_height;
        hc.width = c.width = pic_width, hc.height = c.height = pic_height;
        bmplist = bmparr;
        movie_data.num = num;
    }

    this.start_tracking = function (config, callback) {
        var img = new Image();
        var i = 0;
        data = [];
        (function loop() {
            img.src = bmplist[i];
            img.onload = function () {
                cc.drawImage(img, 0, 0);

                data[i] = cc.getImageData(0, 0, pic_width, pic_height);
                i++;
                if (i <= 2) {
                    setTimeout(loop, 0);
                }
                else {
                    search_target(function (target) {
                        try {
                            callback(create_data(target));
                        } catch (e) {
                            alert(e.message);
                        }
                    }, config);
                }
            }
        })();
    }

    //canvas
    var c, cc, hc, hcc, bmplist, movie_data = {};
    //画像データ
    var data;

    var draw_rect = this.draw_rect = function (sx, sy, tx, ty, color) {
        cc.strokeStyle = color;
        cc.strokeRect(sx, sy, tx - sx + 1, ty - sy + 1);
    }

    bin_draw = function (color_arr, context) {
        var imagedata = context.createImageData(pic_width, pic_height), j;
        for (var i = 0; i < array_length; i++) {
            j = i * 4;
            imagedata.data[j] =
            imagedata.data[j + 1] =
            imagedata.data[j + 2] = color_arr[Math.floor(i / pic_width)][i % pic_width] ? 255 : 0;
            imagedata.data[j + 3] = 255;
        }
        context.putImageData(imagedata, 0, 0);
    }

    //２値画像の平滑化(8近傍)
    var smooth = {
        gain: function (arr) {
            var res = [];
            for (var i = 0; i < pic_height; i++) {
                res[i] = [];
                for (var j = 0; j < pic_width; j++) {
                    if (i == 0 || i == pic_height - 1 || j == 0 || j == pic_width - 1) {
                        res[i][j] = 0;
                    } else if (arr[i - 1][j - 1] ||
                        arr[i - 1][j] ||
                        arr[i - 1][j + 1] ||
                        arr[i][j - 1] ||
                        arr[i][j] ||
                        arr[i][j + 1] ||
                        arr[i + 1][j - 1] ||
                        arr[i + 1][j] ||
                        arr[i + 1][j + 1]) {
                        res[i][j] = 1;
                    } else {
                        res[i][j] = 0;
                    }
                }
            }
            return res;
        },
        loss: function (arr) {
            var res = [];
            for (var i = 0; i < pic_height; i++) {
                res[i] = [];
                for (var j = 0; j < pic_width; j++) {
                    if (i == 0 || i == pic_height - 1 || j == 0 || j == pic_width - 1) {
                        res[i][j] = 0;
                    } else if (arr[i - 1][j - 1] &&
                        arr[i - 1][j] &&
                        arr[i - 1][j + 1] &&
                        arr[i][j - 1] &&
                        arr[i][j] &&
                        arr[i][j + 1] &&
                        arr[i + 1][j - 1] &&
                        arr[i + 1][j] &&
                        arr[i + 1][j + 1]) {
                        res[i][j] = 1;
                    } else {
                        res[i][j] = 0;
                    }
                }
            }
            return res;
        },
        opening: function (arr, n) {
            var are = [arr];
            for (var i = 0; i < n; i++) {
                are[i + 1] = this.loss(are[i]);
            }
            are[0] = are[n];
            for (var i = 0; i < n; i++) {
                are[i + 1] = this.gain(are[i]);
            }
            return are[n];
        },
        closing: function (arr, n) {
            var are = [arr];
            for (var i = 0; i < n; i++) {
                are[i + 1] = this.gain(are[i]);
            }
            are[0] = are[n];
            for (var i = 0; i < n; i++) {
                are[i + 1] = this.loss(are[i]);
            }
            return are[n];
        }
    }

    //フレーム間差分
    var frame_dif = function (pic_arr, th) {
        var dif_arr = [[], []], res = [], i, j, k;

        for (i = 0; i < array_length; i++) {
            j = i << 2;
            dif_arr[0][i] = [
                Math.abs(pic_arr[1].data[j] - pic_arr[0].data[j]),
                Math.abs(pic_arr[1].data[j + 1] - pic_arr[0].data[j + 1]),
                Math.abs(pic_arr[1].data[j + 2] - pic_arr[0].data[j + 2])
            ];
            dif_arr[1][i] = [
                Math.abs(pic_arr[2].data[j] - pic_arr[1].data[j]),
                Math.abs(pic_arr[2].data[j + 1] - pic_arr[1].data[j + 1]),
                Math.abs(pic_arr[2].data[j + 2] - pic_arr[1].data[j + 2])
            ];
            dif_arr[0][i] = !(dif_arr[0][i][0] < th && dif_arr[0][i][1] < th && dif_arr[0][i][2] < th);
            dif_arr[1][i] = !(dif_arr[1][i][0] < th && dif_arr[1][i][1] < th && dif_arr[1][i][2] < th);
        }

        for (i = 0, k = 0; i < pic_height; i++) {
            res[i] = [];
            for (var j = 0; j < pic_width; j++, k++) {
                res[i][j] = dif_arr[0][k] && dif_arr[1][k];
            }
        }
        return res;
    }

    //2次元配列(行列)を返す  arr[H][W]=initv
    var td_array = function (W, H, initv) {
        var arr = new Array(H);
        for (var i = 0; i < H; i++) {
            arr[i] = new Array(W);
            if (initv !== undefined)
                for (var j = 0; j < W; j++)
                    arr[i][j] = initv;
        }
        return arr;
    }

    var Label = function (num) {
        this.num = num;
        this.minx = pic_width;
        this.maxx = 0;
        this.miny = pic_height;
        this.maxy = 0;
        this.npixel = 0;
        this.id = -1;
    }
    Label.prototype.add = function (x, y) {
        this.maxx = (this.maxx > x) ? this.maxx : x;
        this.maxy = (this.maxy > y) ? this.maxy : y;
        this.minx = (this.minx < x) ? this.minx : x;
        this.miny = (this.miny < y) ? this.miny : y;
        this.npixel++;
    }
    //矩形をくっつける
    Label.prototype.marge = function (_label) {
        this.maxx = (this.maxx > _label.maxx) ? this.maxx : _label.maxx;
        this.maxy = (this.maxy > _label.maxy) ? this.maxy : _label.maxy;
        this.minx = (this.minx < _label.minx) ? this.minx : _label.minx;
        this.miny = (this.miny < _label.miny) ? this.miny : _label.miny;
        this.npixel += _label.npixel;
    }

    //ラベリング　ピクセル数の少ないものは排除する
    var labeling = function (arr, W, H) {
        var num = 0, arr2, res = [];

        var search = function (x0, y0, num) {
            var list = [[x0, y0]], x, y, xy;
            while (list.length != 0) {
                //[x, y] = list.pop();
                xy = list.pop();
                x = xy[0];
                y = xy[1];

                if (arr2[y][x] == 1) continue;
                arr2[y][x] = 1;
                res[num].add(x, y);

                if (x + 1 != W && arr[y][x + 1] == 1 && arr2[y][x + 1] == 0)
                    list.push([x + 1, y]);
                if (x - 1 != -1 && arr[y][x - 1] == 1 && arr2[y][x - 1] == 0)
                    list.push([x - 1, y]);
                if (y + 1 != H && arr[y + 1][x] == 1 && arr2[y + 1][x] == 0)
                    list.push([x, y + 1]);
                if (y - 1 != -1 && arr[y - 1][x] == 1 && arr2[y - 1][x] == 0)
                    list.push([x, y - 1]);
            }
        }

        arr2 = td_array(W, H, 0);
        for (var i = 0; i < H; i++) {
            for (var j = 0; j < W; j++) {
                if (arr[i][j] == 1 && arr2[i][j] == 0) {
                    res.push(new Label(num));
                    search(j, i, num);
                    num++;
                }
            }
        }

        for (var i = res.length - 1; i >= 0; i--) {
            if (res[i].npixel <= 36) {
                res.splice(i, 1);
            }
        }

        return res;
    }

    //近い矩形同士をくっつける
    var connect_label = function (labels, near, noise_np) {
        var i, j, k;
        var list = [];
        for (i = 0; i < labels.length; i++) {
            list[i] = [i];
        }
        list.marge = function (a, b) {
            var i;
            for (i = 0; i < this[b].length; i++) {
                this[a].push(this[b][i]);
                if (this[b][i] != b) {
                    this[this[b][i]] = a;
                }
            }
            this[b] = a;
        }
        for (i = 0; labels[i] !== undefined; i++) {
            if (labels[i].id == -1) { labels[i].id = i; }
            for (j = i + 1; labels[j] !== undefined; j++) {
                //矩形が近いかどうか
                if (labels[i].minx - near <= labels[j].maxx &&
                    labels[i].maxx + near >= labels[j].minx &&
                    labels[i].miny - near <= labels[j].maxy &&
                    labels[i].maxy + near >= labels[j].miny) {
                    if (typeof list[i] == "number") {
                        if (typeof list[j] == "object") {
                            list.marge(list[i], j);
                        } else {
                            if (list[i] < list[j]) {
                                list.marge(list[i], list[j]);
                            } else if (list[i] > list[j]) {
                                list.marge(list[j], list[i]);
                            }
                        }
                    } else {
                        if (typeof list[j] == "object") {
                            list.marge(i, j);
                        } else {
                            if (i < list[j]) {
                                list.marge(i, list[j]);
                            } else if (i > list[j]) {
                                list.marge(list[j], i);
                            }
                        }
                    }
                }
            }
        }
        for (i = 0; i < list.length; i++) {
            if (typeof list[i] == "object") {
                for (j = 0; j < list[i].length; j++) {
                    if (list[i][j] !== i)
                        labels[i].marge(labels[list[i][j]]);
                }
            }
        }

        var judge_size = function (label) {
            var s = (label.maxx - label.minx) * (label.maxy - label.miny);
            if (s < (noise_np[0] + (noise_np[0] + noise_np[1]) * (label.maxy + label.miny) / (2 * pic_height)))
                return true;
            else return false;
        }

        for (i = list.length - 1; i >= 0 ; i--) {
            if (typeof list[i] == "number" ||
                judge_size(labels[i])) {
                labels.splice(i, 1);
            }
        }
    }

    //閾値を求める
    otu_th = function (arr, W, H) {
        var t, val = 0, tmp;
        //n:画素数,m:平均
        var n1, n2, m1, m2;
        var histogram = new Array(101);
        for (var i = 0; i < histogram.length; i++) {
            histogram[i] = 0;
        }
        var ave = 0;
        for (var i = 0; i < H; i++) {
            for (var j = 0; j < W; j++) {
                histogram[Math.round(arr[i][j])]++;
            }
        }

        for (var i = 0; i <= 100; i++) {
            n1 = 0, n2 = 0, m1 = 0, m2 = 0;
            for (var j = 0; j <= i; j++) {
                n1 += histogram[j];
                m1 += histogram[j] * j;
            }
            m1 /= n1;
            for (var j = i + 1; j <= 100; j++) {
                n2 += histogram[j];
                m2 += histogram[j] * j;
            }
            m2 /= n2;
            if ((tmp = n1 * n2 * Math.pow(m1 - m2, 2)) > val) {
                t = i;
                val = tmp;
            }
        }

        return t;

    }

    //フレーム間差分→平滑化→ラベリング
    var search_target = function (callback, config) {
        var pic_num = 1, target = [], num = movie_data.num, img = new Image();
        setTimeout(function () {
            var tmp = [], loop = arguments.callee;
            try {
                //フレーム間差分を使って移動物体の場所を検知
                tmp[0] = frame_dif(data, config.frame_th);

                //2値画像の平滑化
                tmp[1] = smooth.opening(tmp[0], 1);
                tmp[2] = smooth.closing(tmp[1], 3);

                tmp[3] = labeling(tmp[2], pic_width, pic_height);

                cc.strokeStyle = "rgb(255,0,0)";
                cc.lineWidth = 2;
                for (var i = 0; i < tmp[3].length; i++) {
                    cc.strokeRect(tmp[3][i].minx, tmp[3][i].miny
                        , (tmp[3][i].maxx + 1 - tmp[3][i].minx), (tmp[3][i].maxy + 1 - tmp[3][i].miny));
                }

                connect_label(tmp[3], config.labeling_near, config.noise_np);

                cc.strokeStyle = "rgb(0,0,255)";
                cc.lineWidth = 2;
                for (var i = 0; i < tmp[3].length; i++) {
                    cc.strokeRect(tmp[3][i].minx, tmp[3][i].miny
                        , (tmp[3][i].maxx + 1 - tmp[3][i].minx), (tmp[3][i].maxy + 1 - tmp[3][i].miny));
                }

                target[pic_num - 1] = tmp[3];
                tmp = null;
            } catch (e) {
                alert("search_target:" + e.message);
            }
            //終了判定
            pic_num++;
            if (pic_num < num ) {
                img.src = bmplist[pic_num + 1];
                img.onload = function () {
                    cc.drawImage(img, 0, 0);
                    data.shift();
                    data.push(cc.getImageData(0, 0, pic_width, pic_height));
                    setTimeout(loop, 0);
                }
            }
            else {
                //終了
                callback(target);
            }
        }, 0);
    }

    //フレーム間で同じ物体のものをつなげる
    var create_data = function (data) {
        var i, j, k, res = [], n = 0, f = false;

        //同じ物体だったら true
        var judge_dif = function (data1, data2) {
            if (data1.minx <= data2.maxx &&
                    data1.maxx >= data2.minx &&
                    data1.miny <= data2.maxy &&
                    data1.maxy >= data2.miny)
                return true;
            else
                return false;
        }

        for (i = 0; i < data[0].length; i++) {
            data[0][i].num = n;
            res.push(new move_object(0));
            res[n].addf(data[0][i].minx, data[0][i].maxx,
            data[0][i].miny, data[0][i].maxy, 0);
            n++;
        }

        for (i = 1; i < data.length; i++) {
            for (j = 0; j < data[i].length; j++) {
                //ここで前のフレームの矩形情報と比較して同じ物体か調べる
                f = false;
                for (k = 0; k < data[i - 1].length; k++) {
                    if (judge_dif(data[i][j], data[i - 1][k])) {
                        data[i][j].num = data[i - 1][k].num;
                        if (res[data[i - 1][k].num].finalf == i) {
                            res[data[i - 1][k].num].ref(data[i][j].minx, data[i][j].maxx,
                                data[i][j].miny, data[i][j].maxy);
                        } else {
                            res[data[i - 1][k].num].addf(data[i][j].minx, data[i][j].maxx,
                            data[i][j].miny, data[i][j].maxy, i);
                        }
                        f = true;
                        break;
                    }
                }
                if (!f) {
                    data[i][j].num = n;
                    res.push(new move_object(i));
                    res[n].addf(data[i][j].minx, data[i][j].maxx,
                    data[i][j].miny, data[i][j].maxy, i);
                    n++;
                }
            }
        }

        for (var i = res.length - 1; i >= 0; i--) {
            if (res[i].startf == res[i].finalf) {
                res.splice(i, 1);
            }
        }

        for (i = 0; i < res.length; i++) {
            res[i].startf++;
            res[i].finalf++;
            for (j = 0; j < res[i].pos.length; j++) {
                res[i].pos[j].num++;
            }
        }

        return res;
    }

    var move_object = function (num) {
        this.startf = num;
        this.finalf = num;
        this.pos = new Array();
    }
    move_object.prototype.addf = function (minx, maxx, miny, maxy, num) {
        this.pos.push({
            num: num,
            minx: minx,
            maxx: maxx,
            miny: miny,
            maxy: maxy
        });
        this.finalf = num;
    }
    move_object.prototype.ref = function (minx, maxx, miny, maxy) {
        var arr = this.pos.pop();
        arr.maxx = (arr.maxx > maxx) ? arr.maxx : maxx;
        arr.maxy = (arr.maxy > maxy) ? arr.maxy : maxy;
        arr.minx = (arr.minx < minx) ? arr.minx : minx;
        arr.miny = (arr.miny < miny) ? arr.miny : miny;
        this.pos.push(arr);
    }

    this.draw_image = function (pic_num, callback) {
        var img = new Image();
        img.src = bmplist[pic_num];
        img.onload = function () {
            cc.drawImage(img, 0, 0);
            callback();
        }
    }
    var draw_image = this.draw_image.bind(this);

    //canvasの画像を出力
    var out_canvasimage = function (canvas) {
        var img = document.createElement("img");
        var img_src = canvas.toDataURL();
        img.src = img_src;
        document.getElementsByTagName("body").item(0).appendChild(img);
    }

    this.out_rectimage = function (obj, callback) {
        var frame = Math.floor((obj.startf + obj.finalf) / 2);
        var dx = obj.pos[frame - obj.startf].maxx - obj.pos[frame - obj.startf].minx + 1,
            dy = obj.pos[frame - obj.startf].maxy - obj.pos[frame - obj.startf].miny + 1;
        this.draw_image(frame, function () {
            var pic = cc.getImageData(obj.pos[frame - obj.startf].minx, obj.pos[frame - obj.startf].miny, dx, dy);
            hc.width = dx;
            hc.height = dy;
            hcc.putImageData(pic, 0, 0);
            if (dx < dy)
                callback([hc.toDataURL(), dx / dy * 50, 50])
            else
                callback([hc.toDataURL(), 50, dy / dx * 50]);
        })
    }

    this.clear = function () {
        cc.clearRect(0, 0, c.width, c.height);
    }

    constructor();
}
