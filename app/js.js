var gui = require('nw.gui');
var fs = require('fs');
var win = gui.Window.get();

var me, mt, menu, sb, video;
var c_file = "./config.json", config;
window.onload = function () {
    var data = { bmplist: {}, num: 5223, width: 200, height: 200, data: [] };
    mt = new my_tracking("can", "hc");
    me = new myelement();
    menu = new menu_bar();
    sb = new select_box();

    config = JSON.parse(open_file(c_file, "utf-8"));

    menu.add_menu("ファイル", [{
        label: "動画ファイルを開く",
        click: function () {
            document.getElementById("folder_dialog").click();
        }
    }, { type: "separator" }, {
        label: "outputファイルを開く",
        click: function () {
            document.getElementById("open_dialog").click();
        }
    }, {
        label: "outputファイルを保存",
        click: function () {
            document.getElementById("save_dialog").click();
        }
    }, { type: 'separator' }, {
        label: "ファイルを閉じる",
        click: function () {
            location.reload();
        }
    }, {
        label: "終了",
        click: function () {
            window.close();
        }
    }]);

    menu.add_menu("機能", [{
        label: "動物体を検出する",
        click: function () {
            mt.start_tracking(config, function (res) {
                data.data = res;
                document.getElementById("output_box").innerHTML = JSON.stringify(res);
                for (var i = 0; i < res.length; i++) {
                    sb.add(res[i]);
                }
                video.dis_frame(0);
            });
        }
    }, {
        label: "検出の設定",
        click: function () {
            gui.Window.open('config.html', { focus: true, width: 600, height: 400 }, function (win) {
                win.on('closed', function () {
                    config = JSON.parse(open_file(c_file, "utf-8"));
                    win = null;
                });
            });
        }
    }]);

    file_dialog("open_dialog", function (url) {
        file_url = url;
        var data;
        try {
            data = JSON.parse(open_file(url, "utf-8"));
        } catch (e) {
            document.getElementById("output_box").innerHTML = e.message;
        }
        video = new video_pos(data.num);
        mt.read_movie(data.bmplist, data.num, data.width, data.height);
        var res = data.data;
        document.getElementById("output_box").innerHTML = JSON.stringify(res);
        for (var i = 0; i < res.length; i++) {
            sb.add(res[i]);
        }
        video.dis_frame(0);
    });

    file_dialog("save_dialog", function (url) {
        var str = JSON.stringify(data);
        save_file(url, str);
    });

    file_dialog("folder_dialog", function (url) {
        var bmplist = url.split(";");
        data = { bmplist: bmplist, num: bmplist.length - 1, width: 720, height: 576, data: [] };
        mt.read_movie(data.bmplist, data.num, data.width, data.height);
        video = new video_pos(data.num);
        video.dis_frame(0);
    });
    return 0;
}

//動画再生の制御
var video_pos = function (video_length) {
    var bar = document.getElementById("seekbar");

    bar.max = video_length;
    document.getElementById("ttime").innerHTML = video_length;

    this.seekbar_change = function () {
        document.getElementById("ctime").innerHTML = bar.value;
        this.current_time = Number(bar.value);
        mt.draw_image(Number(bar.value), function () {
            sb.draw_rect(Number(bar.value));
        });
    }
    bar.addEventListener("change", this.seekbar_change.bind(this), false);

    this.current_time = 0;
    this.start = function (time) {
        if (!this.stopf) return;
        this.stopf = false;
        if (typeof time == "number") {
            this.current_time = bar.value = time;
        } else {
            this.current_time = Number(bar.value);
        }
        var frame = this.current_time;
        (function loop(This) {
            try {
                document.getElementById("ctime").innerHTML = frame;
                bar.value = frame;
                This.current_time = frame;
                mt.draw_image(frame, function () {
                    sb.draw_rect(Number(frame));
                });
                frame++;
                if (frame == video_length || This.stopf) {
                    this.stopf = true;
                } else {
                    setTimeout(function () { loop(This) }, 200);
                }
            } catch (e) {
                alert(e.message);
            }

        })(this);
    }

    this.dis_frame = function (frame) {
        if (!this.stopf) return;
        document.getElementById("ctime").innerHTML = frame;
        this.current_time = frame;
        bar.value = frame;
        mt.draw_image(frame, function () {
            sb.draw_rect(frame);
        });
    }
    this.stop = function () {
        this.stopf = true;
    }
    this.stopf = true;

    //再生、一時停止ボタンを押したときのあれ
    document.getElementById("video_start").addEventListener("click", this.start.bind(this), false);
    document.getElementById("video_stop").addEventListener("click", this.stop.bind(this), false);
}

var menu_bar = function () {
    var parentElem = document.getElementById("head");
    var list = [];
    this.add_menu = function (name, obj_arr) {
        var Elem = document.createElement("div");
        Elem.innerHTML = name;
        parentElem.appendChild(Elem);
        var menu = new gui.Menu();
        for (var i = 0; i < obj_arr.length; i++) {
            if (obj_arr[i].submenu !== undefined) {
                let subm = new gui.Menu();
                for (var j = 0; j < obj_arr[i].submenu.length; j++) {
                    subm.append(new gui.MenuItem(obj_arr[i].submenu[j]));
                }
                obj_arr[i].submenu = null;
                obj_arr[i].submenu = subm;
            }
            menu.append(new gui.MenuItem(obj_arr[i]));
        }
        Elem.addEventListener("click", function (e) {
            var rect = this.getBoundingClientRect();
            menu.popup(rect.left, rect.top + this.clientHeight);
        }, false);
    }
}

//セレクトボックスのメソッド
var select_box = function () {
    var box = me.resist_elem("select_box"), tar_box = [];
    var data = [], clicklist = [];
    this.add = function (obj) {
        var num = tar_box.length;
        data[num] = obj;

        tar_box.push(me.create("div", box));
        tar_box[num].add_child([
            {
                tag: "a",
                attributes: {
                    innerHTML: ("0000" + num).slice(-4)
                },
                style: {
                    width: "80px",
                    height: "100%",
                    cssFloat: "left"
                }
            },
            {
                tag: "img",
                attributes: {
                    width: 50,
                    height: 50
                },
                style: {
                    backgroundColor: "#000000"
                },
                name: "image"
            }
        ]);
        mt.out_rectimage(data[num], function (res) {
            tar_box[num].child.image.e.width = res[1];
            tar_box[num].child.image.e.height = res[2];
            tar_box[num].child.image.e.src = res[0];
            res = null;
        });
        var obj_arr = [
            {
                label: "最初のフレームから再生",
                click: function () {
                    video.start(data[num].startf);
                    video.dis_frame(data[num].startf);
                }
            }, {
                label: "削除",
                click: function () {
                    delete_(num);
                }
            }, { type: "separator" },
            {
                label: "アプリの終了",
                click: function () {
                    window.close();
                }
            }
        ];

        var menu = new gui.Menu();
        for (var i = 0; i < obj_arr.length; i++) {
            if (obj_arr[i].submenu !== undefined) {
                let subm = new gui.Menu();
                for (var j = 0; j < obj_arr[i].submenu.length; j++) {
                    subm.append(new gui.MenuItem(obj_arr[i].submenu[j]));
                }
                obj_arr[i].submenu = null;
                obj_arr[i].submenu = subm;
            }
            menu.append(new gui.MenuItem(obj_arr[i]));
        }

        tar_box[num].e.addEventListener("contextmenu", function (e) {
            menu.popup(e.clientX, e.clientY);
        }, false);

        tar_box[num].e.addEventListener("click", function () {

            if (!key_state.Ctrl) {
                for (var i = 0; i < tar_box.length; i++) {
                    if (i != num && clicklist[i] == true) {
                        clicklist[i] = false;
                        tar_box[i].e.style.backgroundColor = "darkgrey";
                    }
                }
            }
            clicklist[num] = true;
            this.style.backgroundColor = "red";

            video.dis_frame(video.current_time);
        }, false);
        tar_box[num].e.addEventListener("dblclick", function () {
            video.dis_frame(data[num].startf);
        }, false);

        change_num(function () { num--; });
    }
    this.draw_rect = function (num) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].startf <= num && num <= data[i].finalf) {
                mt.draw_rect(data[i].pos[num - data[i].startf].minx,
                    data[i].pos[num - data[i].startf].miny,
                    data[i].pos[num - data[i].startf].maxx,
                    data[i].pos[num - data[i].startf].maxy, clicklist[i] ? "rgb(255,0,0)" : "rgb(0,255,0)");
            }
        }
    }
    //選択した情報をまとめて削除
    this.delete_mult = function () {
        for (var i = 0; i < data.length; i++) {
            if (clicklist[i])
                this.delete(i);
        }
        video.dis_frame(video.current_time);
    }
    //指定した情報を削除
    var delete_ = this.delete = function (num) {
        box.e.removeChild(tar_box[num].e);
        clicklist.splice(num, 1);
        tar_box.splice(num, 1);
        data.splice(num, 1);
        change_num.change(num);
    }

    //全部削除
    this.delete_all = function () {

    }

    var change_num = function (func) {
        arguments.callee.arr.push(func);
    }
    change_num.arr = [];
    change_num.change = function (num) {
        for (var i = num + 1; i < change_num.arr.length; i++) {
            change_num.arr[i]();
        }
    }

    document.getElementById("can").addEventListener("click", function (e) {
        var x = e.offsetX * mt.W / 640, y = e.offsetY * mt.H / 480;
        var num = video.current_time;
        for (var i = 0; i < data.length; i++) {
            if (data[i].startf <= num && num <= data[i].finalf) {
                if (data[i].pos[num - data[i].startf].minx <= x &&
                    data[i].pos[num - data[i].startf].maxx >= x &&
                    data[i].pos[num - data[i].startf].miny <= y &&
                    data[i].pos[num - data[i].startf].maxy >= y) {
                    tar_box[i].e.click();
                }
            }
        }
    }, false)
}

//ファイルのダイアログを開く
function file_dialog(id, callback) {
    document.getElementById(id).onchange = function () {
        // ファイルが選択されたか
        if (this.value) {
            callback(this.value);
        }
    };
}

//HTMLの要素を簡単に生成する
var myelement = function () {
    var This = this;
    //タグの作成
    this.create = function (tagName, parentElem, elemName) {
        var elem = {};
        elem.e = document.createElement(tagName);
        elem.child = [];
        elem.parent = parentElem;
        /*子要素を簡単に生成できるようにしたい。
        "string":aタグ
        それ以外:
            {tag:"tagName",
             attributes:{},
             style:{},
             (name:"name")
            }
        */
        elem.add_child = function (obj) {
            var a;
            for (var i in obj) {
                if (typeof obj[i] == "string" || typeof obj[i] == "number") {
                    This.create("a", this).e.innerHTML = obj[i];
                } else {
                    a = This.create(obj[i].tag, this);
                    for (var j in obj[i].attributes) {
                        a.e[j] = obj[i].attributes[j];
                    }
                    for (var j in obj[i].style) {
                        a.e.style[j] = obj[i].style[j];
                    }
                    if (obj[i].name !== undefined)
                        a.regist_name(obj[i].name);
                }
            }
        }
        //子要素を削除
        elem.del_child = function (child_name) {
            if (this.child[child_name] !== undefined) {
                this.e.removeChild(this.child[child_name].e);
                if (typeof child_name == "number") {
                    this.child.splice(child_name, 1);
                } else {
                    delete this.child[child_name];
                }
            }
        }
        //自身の要素の削除
        elem.delete = function () {
            parentElem.del_child(this.name);
        }
        //自分に名前を付ける
        elem.regist_name = function (str) {
            if (typeof this.name == "number")
                parentElem.child.splice(this.name, 1);
            else
                if (parentElem.child[str] !== undefined)
                    delete parentElem.child[str];
            parentElem.child[str] = this;
            this.name = str;
        }

        if (elemName !== undefined) {
            elem.name = elemName;
            parentElem.child[elemName] = elem;
        } else {
            elem.name = parentElem.child.length;
            parentElem.child.push(elem);
        }
        parentElem.e.appendChild(elem.e)
        return elem;
    }
    //既存の要素を登録する
    this.resist_elem = function (id) {
        var elem = {};
        elem.e = document.getElementById(id);
        elem.child = [];
        //親はいない
        elem.parent = {};
        elem.name = id;

        elem.add_child = function (obj) {
            var a;
            for (var i in obj) {
                if (typeof obj[i] == "string" || typeof obj[i] == "number") {
                    This.create("a", this).e.innerHTML = obj[i];
                } else {
                    a = This.create(obj[i].tag, this);
                    for (var j in obj[i].attributes) {
                        a.e[j] = obj[i].attributes[j];
                    }
                    for (var j in obj[i].style) {
                        a.e.style[j] = obj[i].style[j];
                    }
                    if (obj[i].name !== undefined)
                        a.regist_name(obj[i].name);
                }
            }
        }
        //子要素を削除
        elem.del_child = function (child_name) {
            if (this.child[child_name] !== undefined) {
                this.e.removeChild(this.child[child_name].e);
                if (typeof child_name == "number") {
                    this.child.splice(child_name, 1);
                } else {
                    delete this.child[child_name];
                }
            }
        }

        return elem;
    }
};

//設定ファイルの読み込み
var a = function () {
    const c_file = "./config.json";
    var config = JSON.parse(open_file(c_file, "utf-8"));
}

//ファイルオープン
function open_file(filepath, encoding) {
    var fileContent = "";
    try {
        var stat = fs.statSync(filepath);
        var fd = fs.openSync(filepath, "r");
        var bytes = fs.readSync(fd, stat.size, 0, encoding);

        fileContent += bytes[0];
        fs.closeSync(fd);

    } catch (e) {
        fileContent = null;
    }
    return fileContent;
};

//ファイルセーブ
function save_file(filepath, contents_data) {
    fs.writeFile(filepath, contents_data, function (err) {
        if (err)
            alert("error:" + err);
    });
}

var key_state = {};
var key_code = [];
{
    let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    key_code[13] = "Enter";
    key_code[16] = "Shift";
    key_code[17] = "Ctrl";
    key_code[18] = "Alt";
    key_code[46] = "Delete";

    for (var i = 0; i < 26; i++) {
        key_code[i + 65] = alphabet[i];
        key_state[key_code[i + 65]] = false;
    }

    key_state[key_code[13]] = false;
    key_state[key_code[16]] = false;
    key_state[key_code[17]] = false;
    key_state[key_code[18]] = false;
    key_state[key_code[46]] = false;

    window.addEventListener("keydown", function (e) {
        key_state[key_code[e.keyCode]] = true;
        if (key_state.Delete) {
            sb.delete_mult();
        }
    }, false);
    window.addEventListener("keyup", function (e) {
        key_state[key_code[e.keyCode]] = false;
    }, false);

}
